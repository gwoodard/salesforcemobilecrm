/*
 * JS for addContact generated by Appery.io
 */

Apperyio.getProjectGUID = function() {
    return 'c388399d-aa09-482e-a54d-8d9503a4c272';
};

function navigateTo(outcome, useAjax) {
    Apperyio.navigateTo(outcome, useAjax);
}

function adjustContentHeight() {
    Apperyio.adjustContentHeightWithPadding();
}

function adjustContentHeightWithPadding(_page) {
    Apperyio.adjustContentHeightWithPadding(_page);
}

function setDetailContent(pageUrl) {
    Apperyio.setDetailContent(pageUrl);
}

Apperyio.AppPages = [{
    "name": "startScreen",
    "location": "startScreen.html"
}, {
    "name": "addContact",
    "location": "addContact.html"
}, {
    "name": "contactList",
    "location": "contactList.html"
}];

function addContact_js() {

    /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'menuIconButton': 'addContact_menuIconButton',
        'firstNameInputAddContact': 'addContact_firstNameInputAddContact',
        'lastNameInputAddContact': 'addContact_lastNameInputAddContact',
        'phoneInputAddContact': 'addContact_phoneInputAddContact',
        'emailInputAddContact': 'addContact_emailInputAddContact',
        'birthdayAddContactLabel': 'addContact_birthdayAddContactLabel',
        'birthdayAddContactPicker': 'addContact_birthdayAddContactPicker',
        'notesTextAreaAddContact': 'addContact_notesTextAreaAddContact',
        'saveContactAddContact': 'addContact_saveContactAddContact',
        'menuItemHomePage': 'addContact_menuItemHomePage',
        'menuItemContactList': 'addContact_menuItemContactList',
        'menuItemAddContact': 'addContact_menuItemAddContact'
    };

    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }

    /*
     * Nonvisual components
     */

    Apperyio.mappings = Apperyio.mappings || {};

    Apperyio.mappings["addContact_create_service_onsuccess_mapping_0"] = {
        "homeScreen": "addContact",
        "directions": []
    };

    Apperyio.mappings["addContact_create_service_onbeforesend_mapping_0"] = {
        "homeScreen": "addContact",
        "directions": [

        {
            "from_name": "addContact",
            "from_type": "UI",

            "to_name": "create_service",
            "to_type": "SERVICE_REQUEST",

            "to_default": {
                "headers": {
                    "Authorization": "{salesforce_access_token}",
                    "Content-Type": "application/json"
                },
                "parameters": {},
                "body": null
            },

            "mappings": [

            {

                "source": "$['lastNameInputAddContact:text']",
                "target": "$['body']['LastName']"

            },

            {

                "source": "$['firstNameInputAddContact:text']",
                "target": "$['body']['FirstName']"

            },

            {

                "source": "$['phoneInputAddContact:text']",
                "target": "$['body']['Phone']"

            },

            {

                "source": "$['emailInputAddContact:text']",
                "target": "$['body']['Email']"

            },

            {

                "source": "$['birthdayAddContactPicker:defaultDateValue']",
                "target_transformation": function(value) {
                    var parts = value.split("/");
                    return parts[2] + "-" + parts[1] + "-" + parts[0];
                },
                "target": "$['body']['Birthdate']"

            },

            {

                "source": "$['notesTextAreaAddContact:text']",
                "target": "$['body']['Description']"

            }

            ]
        }

        ]
    };

    Apperyio.datasources = Apperyio.datasources || {};

    window.create_service = Apperyio.datasources.create_service = new Apperyio.DataSource(Salesforce_Contact_create_service, {
        "onBeforeSend": function(jqXHR) {
            Apperyio.processMappingAction(Apperyio.mappings["addContact_create_service_onbeforesend_mapping_0"]);
        },
        "onComplete": function(jqXHR, textStatus) {

        },
        "onSuccess": function(data) {
            Apperyio.processMappingAction(Apperyio.mappings["addContact_create_service_onsuccess_mapping_0"]);
        },
        "onError": function(jqXHR, textStatus, errorThrown) {}
    });

    Apperyio.CurrentScreen = 'addContact';
    _.chain(Apperyio.mappings).filter(function(m) {
        return m.homeScreen === Apperyio.CurrentScreen;
    }).each(Apperyio.UIHandler.hideTemplateComponents);

    /*
     * Events and handlers
     */

    // On Load
    var addContact_onLoad = function() {
            addContact_elementsExtraJS();

            addContact_deviceEvents();
            addContact_windowEvents();
            addContact_elementsEvents();
        };

    // screen window events


    function addContact_windowEvents() {

        $('#addContact').bind('pageshow orientationchange', function() {
            var _page = this;
            adjustContentHeightWithPadding(_page);
        });

    };

    // device events


    function addContact_deviceEvents() {
        document.addEventListener("deviceready", function() {

        });
    };

    // screen elements extra js


    function addContact_elementsExtraJS() {
        // screen (addContact) extra code

        /* birthdayAddContactPicker */

        addContact_birthdayAddContactPicker_selector = "#addContact_birthdayAddContactPicker";
        addContact_birthdayAddContactPicker_dataPickerOptions = {
            dateFormat: "mm/dd/yy",
            firstDay: 0,

            maxDate: new Date(""),

            minDate: new Date(""),

            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            showOtherMonths: true
        };

        addContact_birthdayAddContactPicker_dataPickerOptions.defaultDate = new Date("09/29/2015");

        Apperyio.__registerComponent('birthdayAddContactPicker', new Apperyio.ApperyMobileDatePickerComponent("addContact_birthdayAddContactPicker", addContact_birthdayAddContactPicker_dataPickerOptions));

    };

    // screen elements handler


    function addContact_elementsEvents() {
        $(document).on("click", "a :input,a a,a fieldset label", function(event) {
            event.stopPropagation();
        });

        $(document).off("click", '#addContact_mobileheader [name="menuIconButton"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    $('[id="addContact_mainMenuList"]').panel("open");

                }
            },
        }, '#addContact_mobileheader [name="menuIconButton"]');

        $(document).off("click", '#addContact_mobilecontainer [name="saveContactAddContact"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    try {
                        create_service.execute({});
                    } catch (e) {
                        console.error(e);
                        hideSpinner();
                    };

                }
            },
        }, '#addContact_mobilecontainer [name="saveContactAddContact"]');

        $(document).off("click", '#addContact_mainMenuList [name="menuItemHomePage"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    Apperyio.navigateTo('startScreen', {
                        reverse: true
                    });

                }
            },
        }, '#addContact_mainMenuList [name="menuItemHomePage"]');
        $(document).off("click", '#addContact_mainMenuList [name="menuItemContactList"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    Apperyio.navigateTo('contactList', {
                        reverse: true
                    });

                }
            },
        }, '#addContact_mainMenuList [name="menuItemContactList"]');
        $(document).off("click", '#addContact_mainMenuList [name="menuItemAddContact"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    Apperyio.navigateTo('addContact', {
                        reverse: true
                    });

                }
            },
        }, '#addContact_mainMenuList [name="menuItemAddContact"]');

    };

    $(document).off("pagebeforeshow", "#addContact").on("pagebeforeshow", "#addContact", function(event, ui) {
        Apperyio.CurrentScreen = "addContact";
        _.chain(Apperyio.mappings).filter(function(m) {
            return m.homeScreen === Apperyio.CurrentScreen;
        }).each(Apperyio.UIHandler.hideTemplateComponents);
    });

    addContact_onLoad();
};

$(document).off("pagecreate", "#addContact").on("pagecreate", "#addContact", function(event, ui) {
    Apperyio.processSelectMenu($(this));
    addContact_js();
});