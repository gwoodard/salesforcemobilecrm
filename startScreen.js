/*
 * JS for startScreen generated by Appery.io
 */

Apperyio.getProjectGUID = function() {
    return 'c388399d-aa09-482e-a54d-8d9503a4c272';
};

function navigateTo(outcome, useAjax) {
    Apperyio.navigateTo(outcome, useAjax);
}

function adjustContentHeight() {
    Apperyio.adjustContentHeightWithPadding();
}

function adjustContentHeightWithPadding(_page) {
    Apperyio.adjustContentHeightWithPadding(_page);
}

function setDetailContent(pageUrl) {
    Apperyio.setDetailContent(pageUrl);
}

Apperyio.AppPages = [{
    "name": "startScreen",
    "location": "startScreen.html"
}, {
    "name": "addContact",
    "location": "addContact.html"
}, {
    "name": "contactList",
    "location": "contactList.html"
}];

function startScreen_js() {

    /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'homePagePictureCarousel': 'startScreen_homePagePictureCarousel',
        'mobilecarouselitem_3': 'startScreen_mobilecarouselitem_3',
        'mobilecarouselitem_4': 'startScreen_mobilecarouselitem_4',
        'mobilecarouselitem_5': 'startScreen_mobilecarouselitem_5',
        'loginSalesforceButton': 'startScreen_loginSalesforceButton'
    };

    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }

    /*
     * Nonvisual components
     */

    Apperyio.mappings = Apperyio.mappings || {};

    Apperyio.datasources = Apperyio.datasources || {};

    Apperyio.CurrentScreen = 'startScreen';
    _.chain(Apperyio.mappings).filter(function(m) {
        return m.homeScreen === Apperyio.CurrentScreen;
    }).each(Apperyio.UIHandler.hideTemplateComponents);

    /*
     * Events and handlers
     */

    // On Load
    var startScreen_onLoad = function() {
            startScreen_elementsExtraJS();

            startScreen_deviceEvents();
            startScreen_windowEvents();
            startScreen_elementsEvents();
        };

    // screen window events


    function startScreen_windowEvents() {

        $('#startScreen').bind('pageshow orientationchange', function() {
            var _page = this;
            adjustContentHeightWithPadding(_page);
        });

    };

    // device events


    function startScreen_deviceEvents() {
        document.addEventListener("deviceready", function() {

        });
    };

    // screen elements extra js


    function startScreen_elementsExtraJS() {
        // screen (startScreen) extra code

        /* homePagePictureCarousel*/
        var homePagePictureCarousel_options = {
            indicatorsListClass: "ui-carousel-indicators",
            showIndicator: true,
            showTitle: true,
            titleBuildIn: false,
            titleIsText: true,
            animationDuration: 250,
            useLegacyAnimation: false,
            enabled: true,
        }
        Apperyio.__registerComponent('homePagePictureCarousel', new Apperyio.ApperyMobileCarouselComponent("startScreen_homePagePictureCarousel", homePagePictureCarousel_options));

        $("#startScreen_mobilecarouselitem_3").attr("reRender", "homePagePictureCarousel");

        $("#startScreen_mobilecarouselitem_4").attr("reRender", "homePagePictureCarousel");

        $("#startScreen_mobilecarouselitem_5").attr("reRender", "homePagePictureCarousel");

    };

    // screen elements handler


    function startScreen_elementsEvents() {
        $(document).on("click", "a :input,a a,a fieldset label", function(event) {
            event.stopPropagation();
        });

        $(document).off("click", '#startScreen_mobilecontainer [name="loginSalesforceButton"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    salesforceLogin();
                    Apperyio.navigateTo('contactList', {
                        reverse: false
                    });

                }
            },
        }, '#startScreen_mobilecontainer [name="loginSalesforceButton"]');

    };

    $(document).off("pagebeforeshow", "#startScreen").on("pagebeforeshow", "#startScreen", function(event, ui) {
        Apperyio.CurrentScreen = "startScreen";
        _.chain(Apperyio.mappings).filter(function(m) {
            return m.homeScreen === Apperyio.CurrentScreen;
        }).each(Apperyio.UIHandler.hideTemplateComponents);
    });

    startScreen_onLoad();
};

$(document).off("pagecreate", "#startScreen").on("pagecreate", "#startScreen", function(event, ui) {
    Apperyio.processSelectMenu($(this));
    startScreen_js();
});