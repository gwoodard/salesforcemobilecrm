/*
 * Service settings
 */
var Salesforce_settings = {
    "client_id": "3MVG9KI2HHAq33RwiAEPA4zhMR36JOw4el6Ly_SvJS7skLu5anGwboyaUvEcHIA7Pj5HsOadAPZ6AGXjc92db",
    "redirect_url": "https://appery.io/app/salesforce/apperyio-salesforce-callback",
    "login_url": "https://login.salesforce.com/services/oauth2/authorize",
    "salesforce_instance_url": "",
    "salesforce_access_token": "",
    "salesforce_api_version": "v34.0"
}

/*
 * Services
 */

var Salesforce_Contact_query_service = new Apperyio.RestService({
    'url': 'https://api.appery.io/rest/1/proxy/tunnel',
    'proxyHeaders': {
        'appery-proxy-url': '{salesforce_instance_url}/services/data/{salesforce_api_version}/query',
        'appery-transformation': 'checkTunnel',
        'appery-key': '1452108425692',
        'appery-rest': 'abb3cadc-447d-4177-b50c-3f6548744790'
    },
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': Salesforce_settings
});

var Salesforce_Contact_read_service = new Apperyio.RestService({
    'url': 'https://api.appery.io/rest/1/proxy/tunnel',
    'proxyHeaders': {
        'appery-proxy-url': '{salesforce_instance_url}/services/data/{salesforce_api_version}/sobjects/Contact/{Id}',
        'appery-transformation': 'checkTunnel',
        'appery-key': '1452108425692',
        'appery-rest': 'abb3cadc-447d-4177-b50c-3f6548744790'
    },
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': Salesforce_settings
});

var Salesforce_Contact_create_service = new Apperyio.RestService({
    'url': 'https://api.appery.io/rest/1/proxy/tunnel',
    'proxyHeaders': {
        'appery-proxy-url': '{salesforce_instance_url}/services/data/{salesforce_api_version}/sobjects/Contact',
        'appery-transformation': 'checkTunnel',
        'appery-key': '1452108425692',
        'appery-rest': 'abb3cadc-447d-4177-b50c-3f6548744790'
    },
    'dataType': 'json',
    'type': 'post',
    'contentType': 'application/json',

    'serviceSettings': Salesforce_settings
});