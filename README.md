# Salesforce Mobile CRM App #

### **About:** ###

A mobile application that will allow end users to enter customer information (First Name, Last Name, Phone, Email, Birthdate) and Notes about the customer. Once entered it will save the information via the Salesforce Developer API to a CRM backend with the entered information. A working Android (.apk) version is available in the source folder along with being online at: 
- https://appetize.io/app/1txxku09wybecybvz9r6pmzg3g 

### **IDE:**
Intel XDK, Appery.io and C9.io

### **Input:** ###
CRM - Customer Relationship Manager Application: Input customer information such as First Name, Last Name, Phone, Email and Birthdate

### **Language/API:** ###
Ionic, Angular, JavaScript, Salesforce API, HTML, CSS